Elgg Documentation
====================

# Developer Guides (開発者ガイド)

プラグインとElggの動作をカスタマイズ

## Don't Modify Core (コアを変更しないでください)

**警告**: Elgg付属の設定ファイル以外を編集しないでください。

カスタムプラグインを作り、Elgg plugin APIを通して挙動を変更してください。

Elggのコアを変更しない主な理由は、プラグインを通すことで良い拡張性が得られるからです。

### It makes it hard to get help (助けを得ることを困難にします)

コアを変更したならば、他の人がシステムを理解することができません。これは、システムをサポートする人々を失望させるでしょう。

### It makes upgrading tricky and potentially disastrous (アップデートを危険で困難なものにします)

セキュリティパッチ、新機能、新しいプラグインAPI、安定性とパフォーマンス向上を得るためには、Elggをアップグレードする必要があります。もし、コアファイルを変更している場合、アップグレードする際に、上書きされないか、互換性があるか非常に注意する必要があります。変更内容が失われたり、互換性がなくなった場合、あなたのサイトは壊れてしまいます。

アップグレードプロセスは非常に複雑なので、正常にアップグレードすることは容易ではありません。

### It may break plugins (プラグインが壊れる)

プラグインを簡単に修正しただけでは、潜在的な問題に気付けない可能性があります。

### Summary (要約)

- Resist the temptation (誘惑に抵抗する)  
既存のファイルを編集すると速くて簡単ですが、複雑になると、サイトの保守性、セキュリティ、安定性のリスクを負います。

- コアを変更すると、後に変更を伝えることや、トラブルを修正するために周りから負担をかけられる恐れがあります。

- Apply these principle to software in general. (一般的なソフトウェア主義に当たります)  
プラグインの製作者は新しいバージョンをリリースします。トラブルを避けるためには、他者のプラグインを修正しないでください。

## Plugins

Elggにプラグインを認識させるには、プラグインのルートディレクトリにstart.phpとmanifest.xmlファイルを作成する必要があります。

### start.php

start.phpファイルはevent listenersとplugin hooksに登録する、ブートストラップです。

### activate.php, deactivate.php

activate.phpとdeactivate.phpファイルは、プラグインのアクティブ化および非アクティブ化の際に実行される手続き型のコードを含んでいます。非アクティブ化されるとき、持続的な管理者通知をするか、サブタイプを登録するか、ガーベッジコレクションを実行するなどのイベントを実行するためにこれらのファイルが一度だけ実行されます。

### manifest.xml

`manifest.xml`ファイルは、プラグインの情報、プラグインの依存関係、オプションの情報が含まれます。これは、管理者エリアのプラグイン画面に表示されます。

### Syntax

マニフェストファイルはUTF-8のXMLファイルです。
全ての要素が`<plugin_manifest>`内に収まります。

```
<?xml version="1.0" encoding="UTF-8" ?>
<plugin_manifest xmlns="http://www.elgg.org/plugin_manifest/1.8">
```

マニフェストの構文は次のようにします:

```
<name>value</name>
```

いくつかの要素を子属性として含むことができます:

```
<parent_name>
    <child_name>value</child_name>
    <child_name_2>value_2</child_name_2>
</parent_name>
```

### Required Elements (必須の要素)

全てのプラグインは、マニフェストファイルに次の要素を定義する必要があります:

- id - プラグインが使用するディレクトリの名前

- name - プラグインの表示名

- author - プラグイン製作者の名前

- version - プラグインのバージョン

- description - プラグインの機能説明、関連情報など

- requires - 各プラグインは、どのバージョンのElggに向けて開発したのかを指定する必要があります。詳細については、プラグインの依存関係ページを参照してください。

### Available Elements (利用できる要素)

上記の要素に加えて、以下の要素が利用可能です:

- blurb - プラグインの短い説明

- category - プラグインのカテゴリ。Plugin Guidelinesに定義されたいずれかを使用することを推奨します。複数のカテゴリを指定できます。

- conflict - プラグインとシステムの矛盾する部分を指定

- copyright - プラグインの著作権情報

- license - プラグインのライセンス情報

- provides - 別のプラグインやPHP拡張モジュールと同じ機能を提供することを明記します。

- screenshot - プラグインのスクリーンショット。構文については、高度な例を参照してください。

- suggest - 他のプラグインに依存している場合、プラグインを提案するために使用します。

- website - プラグインのウェブサイトへのリンク

### Simple Example

最も小さなマニフェストファイル

```
<?xml version="1.0" encoding="UTF-8"?>
<plugin_manifest xmlns="http://www.elgg.org/plugin_manifest/1.8">
    <name>Example Manifest</name>
    <author>Elgg</author>
    <version>1.0</version>
    <description>This is a simple example of a manifest file.</description>

    <requires>
        <type>elgg_release</type>
        <version>1.9</version>
    </requires>
</plugin_manifest>
```

### Advanced example

## Plugin coding guidelines

## Accessibility Guidelines

## Helper functions

## Forms + Actions

コンテンツを作る、更新する、削除する

Elggのフォームはアクションに送信されます。アクションはフォームのために動作を定義します。

このガイドは以下の基本項目の熟知を前提とします。

- Plugins

- Views

- Internationalization

Contents

- Registering actions
  - Permissions
  - Writing action files
  - Customizing actions

- Files and images

- Sticky forms
  - Helper functions
  - Overview
  - Example: User registration
  - Example: Bookmarks

- Ajax

- Security

### Registering actions

actionは使う前に登録しなければいけません。`elgg_register_action`を使い次のようにします:

```
elgg_register_action("example", __DIR__ . "/actions/example.php");
```

`mod/example/actions/example.php`スクリプトは、form内容が`http://localhost/elgg/action/example`に提出された時に実行されます。

**警告**: 多くの新しい開発者の躓くポイントは、actionの為のURLです。URLは常に`/action/` (singular)を使います。`/actions/` (plural)ではありません。しかしながら、actionスクリプトファイルは通常`/actions/` (plural)に保存され拡張性を持ちます。

### Permissions

デフォルトでは、actionはログインしているユーザだけが利用可能です。

ログアウトしたユーザがactionを利用できるようにするには3つめのパラメータに`"public"`を指定します:

```
elgg_register_action("example", $filepath, "public");
```

管理者だけにactionを制限するならば、最後のパラメータを`"admin"`にします:

```
elgg_register_action("example", $filepath, "admin");
```

### Writing action files

パラメータを受取るためには`get_input`関数を使います:

```
$field = get_input('input_field_name', 'default_value');
```

entityを読み込むためにDatabase apiを使うことができます。その結果として、actionが実行されます。

actionが完了したら、`forward`関数を使ってページをリダイレクトできます。

```
forward('url/to/forward/to');
```

例えば、ユーザプロフィールに転送するには:

```
$user = elgg_get_logged_in_user_entity();
forward($user->getURL());
```

URLはElgg rootからの相対パスを使えます:

```
$user = elgg_get_logged_in_user_entity();
forward("/example/$user->username");
```

`REFERRER`を使うことで参照ページにリダイレクトします:

```
forward(REFERRER);
forware(REFERER); // equivalent
```

警告やエラー、フィードバックや`register_error`を受け取りたいなら、`system_message`を使うことでユーザのactionの状態を得られます。

```
if ($success) {
    system_message(elgg_echo('actions:example:success'));
} else {
    register_error(elgg_echo('actions:example:error'));
}
```

### Customizing actions

actionを実行する前に、Elggはトリガーをセットします:

```
$result = elgg_trigger_plugin_hook('action', $action, null, true);
```

$actionはactionです。呼ばれたら開始します。もし、hookが`false`を返したならば、actionは実行されません。

## Database

## Internationalization

## Menus

Elggはサイトの至るところにメニューを作るためのコードを含んでいます。

全てのメニュー項目には、名前が必要です。テーマのためのhookを提供するだけでなく、簡単に書き換えたり小細工するためには名前が必須となります。

Contents

- Basic usage

- Advancedusage

- Creating a new menu

- Theming

### Basic usage

基本的な機能はこれら２つの関数によって成されます。

- elgg_register_menu_item() メニューに項目を追加します。
- elgg_unregister_menu_item() メニューから項目を取り除きます

基本的に、プラグインのinit関数からこれらを呼びます。

#### Examples

```
// Add a new menu item to the site main menu
elgg_register_menu_item('site', array(
    'name' => 'itemname',
    'text' => 'This is text of the item',
    'href' => '/item/url',
));
```

```
// Remove the "Elgg" logo from the topbar menu
elgg_unregister_menu_item('topbar', 'elgg_logo');
```

### Advanced usage

plugin hooksによりmenuをよりコントロールすることができます。そしてそれはElggMenuItemクラスによって提供されます。

**Menuを修正するのに使われる2つのhookがあります:**

- `'register', 'menu:<menu name>'' 修正する項目を追加します(特に動的なメニューです)

- `'prepare', 'menu:<menu name>'` 表示する前にメニューの構造を修正します

plugin hook handlerを登録した時に、メニューの名前を<menu name>部分で置き換えます。

menu handlerに通される３つ目のパラメータはElggのコアによって登録された全てのメニュー項目を含んでいます。そして、他のプラグインはこれらを有効にします。メニュー項目のプロパティを相互利用するために、classメソッドを使うことでメニューアイテムを繰返し利用することができます。

#### Examples

**Example 1:** `owner_block`メニューにおいて"albums"というメニュー項目が呼ばれたときのURLを変更する。

```
/**
 * Initialize the plugin
 */
function my_plugin_init() {
    // Register a plugin hook handler for the owner_block menu
    elgg_register_plugin_hook_handler('register', 'menu:owner_block', 'my_owner_block_menu_handler');
}

/**
 * Change the URL of the "Albums" menu item in the owner_block menu
 */
function my_owner_block_menu_handler($hook, $type, $menu, $return) {
    $owner = $params['entity'];

    // Owner can be either user or a group, so we
    // need to take both URLs into consideration;
    switch($owner->getType()) {
        case 'user':
            $url = "album/owner/{$owner->guid}";
            break;
        case 'group':
            $url = "album/group/{$owner->guid}";
            break;
    }

    foreach($menu as $key => $item) {
        if ($item->getName() == 'albums') {
            // Set the new URL
            $item->setURL($url);
            break;
        }
    }

    return $menu;
}
```

**Example 2**: `ElggBlog`オブジェクトのために`entity`メニューを修正する。

- サムネイルアイコンを削除する

- "Edit"テキストをアイコンに変更する

```
/**
 * Initialize the plugin
 */
function my_plugin_init() {
    // Register a plugin book handler for the entity menu
    elgg_register_plugin_hook_handler('register', 'menu:entity', 'my_entity_menu_handler');
}

/**
 * Customize the entity menu for ElggBlog objects
 */
function my_entity_menu_handler($hook, $type, $menu, $params) {
    // The entity can be found from the $params parameter
    $entity = $params['entity'];

    // We want to modify only the ElggBlog objects, so we
    // return immediately if the entity is something else
    if(!$entity instanceof ElggBlog) {
        return $menu;
    }

    foreach($menu as $key => $item) {
        switch($item->getName()) {
            case 'likes':
                // Remove the "likes" menu item
                unset($menu[$key]);
                break;
            case 'edit':
                // Change the "Edit" text into a custom icon
                $item->setText(elgg_view_icon('pencil'));
                break;
        }
    }

    return $menu;
}
```

### Creating a new menu

Elggはデフォルトで複数の異なったメニューを提供しています。しかし、既存のメニューに合わないメニュが必要になるときがあるかもしれません。もしそんなケースになったら、`elgg_view_menu()`関数を使ってメニューを作ることができます。メニューをディスプレイに表示したい場合、この関数をviewから呼ぶ必要があります。

**Example**:"my_menu"を呼んでメニューを表示してください。メニューはアルファベット順に表示されます。

```
echo elgg_view_menu('my_menu', array('sort_by' => 'title'));
```

新しい項目をメニューに追加できます:

```
elgg_register_menu_item('my_menu', array(
    'name' => 'my_page',
    'href' => 'path/to/my_page',
    'text' => 'elgg_echo('my_plugin:my_page'),
));
```

さらに、menuで使っているhookを修正することができます。
`'register', 'menu:my_menu'` and `'prepare', 'menu:my_menu'`



## Notifications

## River

## Page handler

Elggはページハンドラを経由して、プラグインのページを管理する機能を提供しています。`http://yoursite/your_plugin/section`のようなカスタムURLを可能にします。プラグインにページハンドラを追加するには、`start.php`ファイルにハンドラ関数`elgg_register_page_handler()`を記述する必要があります。

```
elgg_register_page_handler('your_plugin', 'your_plugin_page_handler');
```

プラグインのページハンドラには、2つのパラメータを渡します。

- an array containing the sections of the URL exploded by '/'. With this informatino the handler will be able to apply any logic necessary, for example loading the appropriate view and returning its contents.

- the handler, this is the handler that is currently used (in our example `your_plugin`). If you don't register multiple page handlers to the same function you'll never need this.

### Code flow

プラグイン内のpagesは、プラグインのディレクトリの`pages/`に格納され、一つのページハンドラを介して提供されるべきす。Elggの`engine/start.php`ファイルを組込む必要はありません。これらのファイルの目的は、ユーザが見ているページを形成するために異なるviewsからの出力を編成することです。

1. ユーザの要求 /plugin_name/section/entity

2. Elggは確認します。`plugin_name`がpage handlerにより登録され、その関数が呼ばれたならば、最初の要素として`array('section', 'entity')を解析します。

3. page handler関数がページに表示することを決定します。オプションとして、いくつかの値をセットして、`plugin_name/pages/plugin_name/`の正しいページを含みます。

4. 含んでいるファイルは多くの異なったviewsと結合されます。`elgg_view_layout()`と`elgg_view_page()`のような形式の関数が呼ばれます。そして、echoで最終的に出力されます。

5. ユーザが表示されたページを見ます。

URLに基づいた構文はありません。しかし、Elggのスタンダートなコーディングをすることを勧めます。

## Routing

## Page ownership

## Gatekeeper

Gatekeeper関数は、アクセス制御規則を適用することにより、コードが実行される方法を管理します。

### elgg_gatekeeper()

この関数は、現在見ているユーザがログインしていない場合に、ユーザを前のページへ転送します。

これは、ログインしていないユーザから見られるのを阻止するために、プラグインページにおいて利用することができます。

**Note**: Elgg1.9から、この関数はgatkeeper()と呼ばれています。

### elgg_admin_gatekeeper()

elgg_gatekeeper()と同じです。しかし、管理ユーザだけはページを見ることができます。

**Note**: Elgg1.9から、この関数はadmin_gatekeeper()と呼ばれています。

### action_gatekeeper()

これは、Forms + Actionsにおいて使うべきです。フォーム攻撃からactionを守ります。

**Note**:
Elgg1.8において、この関数は全ての登録されたactionから呼ばれています。
あなたのactionにおいてこの関数を呼ぶ必要はありません。もし、他のページとactionを守っていると分かるようにしたいなら、この関数を呼んでください。

## Widgets

## Views

## Context

## Themes

## JavaScript

## Plugin settings

## Permissions Check

## Authentication

## Walled Garden

## Web services

## Upgrading Plugins

## List of events in core

## List of plugin hooks
